#!/bin/bash

# First run installation script to initialize a new system as part of my dotfiles management
# Though process: This script is to run and create a check file to prevent another initialization
# Check file will need to be removed for the script to run again
# I will most likely break some things as I learn this :/

# Set variables
DATE=`date +"%Y-%m-%d"`
CDATE="# Created on $DATE using ~/.dotfiles/bin/systeminit.sh"
FDATE=`date +"%Y%m%d%H%M%S"`
LOGFILE=~/.systeminit.log
SETENV=~/.profile/sysinitenv.sh


# Check to see if firstrun has already been implemented
if [ -f $LOGFILE ]; then
	printf "You've already run systeminit.sh\n"
	printf "To run again remove the \".systeminit.log\" file\n"
	exit
else	
	echo "# Beginning system initialization\n" > $LOGFILE
	printf "$CDATE\n" >> $LOGFILE
fi


# Identify which OS we're on because I have fucking ADHD and can't stay on one OS for long SMDH
# *that and I like to play with VMs

if [ -f /etc/os-release ]; then
	OS=`cat /etc/os-release | grep -w ID | awk -F "=" '{print $2}'`
	if [[ "$OS" =~ (pop|ubuntu|debian) ]]; then
		export OS=$OS
		sh $HOME/.dotfiles/packages.deb
	elif [[ "$OS" =~ (fedora|centos|redhat|rocky|nobara) ]]; then
		export OS=$OS
		sh $HOME/.dotfiles/packages.rpm
	else
		OS="unknown"
		printf "OS detection failed\n" >> $LOGFILE
		exit
	fi

	printf "OS detection determined OS=$OS\n" >> $LOGFILE
fi


# Final step to prevent an accidental second execution
printf "Part of dotfiles management. Delete this file to run systeminit.sh again" >> $LOGFILE 
