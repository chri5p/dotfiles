### Instructions

# ./install to run parameters from install.conf.yaml
$HOME/.dotfiles/install

# After initial bootstrap install will need to run bin/firstrun.sh manually
# Part of the bootstrap should put ~/bin in the PATH so can run it from there
$HOME/bin/systeminit.sh

# Changed .bash_aliases to .aliases and linked both back to aliases in source git
