# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# NOTE: This did not seem to work on Fedora, not sure why.
# By not "working" it's meant that it was not processing the .profile file
# Commenting out and moving on from this.
# Will put in aliases instead
# set OS variable to be used in some scripts and functions
#if [ -f /etc/os-release ]; then
#	OS=`cat /etc/os-release | grep -w ID | awk -F "=" '{print $2}'`
#	if [[ "$OS" =~ (pop|ubuntu|debian) ]]; then
#		OS=$OS
#	elif [[ "$OS" =~ (fedora|centos|redhat|rocky) ]]; then
#		OS=$OS
#	else
#		OS="unknown"
#		exit
#	fi
#fi

