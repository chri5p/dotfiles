#!/bin/bash

# DNF performance tweaks
# Check to see if tweaks exist and add if they do not
DNFCONF='/etc/dnf/dnf.conf'
if [[ -f $DNFCONF ]]; then
	if [[ ! `grep "fastestmirror" $DNFCONF` ]]; then
		sudo echo 'fastestmirror=1' >> /etc/dnf/dnf.conf
	fi
	if [[ `! grep "max_parallel_downloads" $DNFCONF` ]]; then
		sudo echo 'max_parallel_downloads=10' >> /etc/dnf/dnf.conf
	fi
	if [[ ! `grep "defaultyes" $DNFCONF` ]]; then
		sudo echo 'defaultyes=True' >> /etc/dnf/dnf.conf
	fi
fi

# Fedora repo update list
sudo dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y groupupdate core
sudo dnf -y upgrade --refresh

# Grab fedora packages and run through installing them
if [[ -f ~/dotfiles/packages.rpm ]]; then
	packages=$(cat ~/dotfiles/packages.rpm)
	for i in $packages
	do
	   sudo dnf -y install $i
	done
fi
